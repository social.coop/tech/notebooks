Notebooks for analyzing things to do with social.coop.

If you have Python3 installed on your computer you should be able to get going with:

```
pip3 install -r requirements.txt
jupyter lab
```

Note: this repository is PUBLIC so do not store personal information in the repository itself. If this proves to be burdensome it can be made private.
